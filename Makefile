IMAGE=ghcr.io/utopia-planitia/renovate-bot-extended-image:latest

run: tmp
	docker pull ${IMAGE}
	docker run \
	-ti \
	--rm \
	--env-file=env \
	-v "${PWD}/config.js:/usr/src/app/config.js" \
	-v ${PWD}/tmp:/tmp \
	${IMAGE} \

#	--dry-run=true
#	--git-lab-automerge=false --automerge=false \
#	--enabled-managers=kubernetes


tmp:
	mkdir tmp
	sudo chmod 777 tmp
