module.exports = {
  platform: "gitlab",
  onboarding: true,
  onboardingConfig: {
    $schema: "https://docs.renovatebot.com/renovate-schema.json",
    extends: ["config:best-practices"],
    pinDigests: true,
    prHourlyLimit: 0,
    automerge: true,
    automergeType: "pr",
    platformAutomerge: true,
    labels: ["renovate-bot"],
    dependencyDashboard: false,
    postUpgradeTasks: { commands: [], fileFilters: ["**/**"] }
  },
  configMigration: true,
  autodiscover: true,
  autodiscoverFilter: "utopia-planitia/**",
  allowPostUpgradeCommandTemplating: true,
  allowedPostUpgradeCommands: [""],
  persistRepoData: true,
  repositoryCache: "enabled",
  allowCustomCrateRegistries: true,
  allowScripts: true,
  exposeAllEnv: true,
  detectHostRulesFromEnv: true,
  // the registry aliases only work when the long form of the Docker Hub images is used
  // for example "docker.io/library/alpine" instead of "alpine" or "library/alpine"
  // but using the registry aliases breaks the replacements in the package rules that
  // would convert the short form image names into the long form
  registryAliases: {
    "docker.io/library": "public.ecr.aws/docker/library",
    "index.docker.io/library": "public.ecr.aws/docker/library",
    "registry-1.docker.io/library": "public.ecr.aws/docker/library",
  },
  packageRules: [
    /*
    // the docker hub replacement package rules do not work when registryAliases are used for docker hub
    {
      description: "expand short names of Docker Hub official images",
      matchDatasources: ["docker"],
      matchDepPatterns: ["^[a-z-]+$"],
      replacementNameTemplate: "docker.io/library/{{{ packageName }}}",
    },
    {
      description: "expand short names of Docker Hub non-official images",
      matchDatasources: ["docker"],
      matchDepPatterns: ["^[a-z-]+\\/[a-z-]+$"],
      replacementNameTemplate: "docker.io/{{{ packageName }}}",
    },
    */
    // update "-buster" and "-bullsyeye" golang images to "-bookworm"
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "/^(?:buster|bullseye)$/",
      replacementVersion: "bookworm"
    },
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "/^1-(?:buster|bullseye)$/",
      replacementVersion: "1-bookworm"
    },
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "1.20-buster",
      replacementVersion: "1.20-bookworm"
    },
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "1.20.5-buster",
      replacementVersion: "1.20.5-bookworm"
    },
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "1.22-bullseye",
      replacementVersion: "1.22-bookworm"
    },
    {
      matchDatasources: [
        "docker"
      ],
      matchDepNames: [
        "golang",
        "library/golang",
        "docker.io/library/golang",
        "public.ecr.aws/docker/library/golang"
      ],
      matchCurrentValue: "1.22.4-bullseye",
      replacementVersion: "1.22.4-bookworm"
    }
  ],
  customManagers: [
    {
      description: "Update container images in Helmfile values. See https://regex101.com/r/jIYiqe/1 for examples.",
      customType: "regex",
      fileMatch: [
        "(^|/)helmfile.ya?ml(\\.tpl)?$",
      ],
      datasourceTemplate: "docker",
      matchStrings: [
        "[iI]mage:(?:\\s+#[^\\n]*)*\\n\\s+registry:\\s+[\"']?(?<registryUrl>[^\\s\"']+)[\"']?(?:\\s+#[^\\n]*)*\\n\\s+repository:\\s+[\"']?(?<depName>[^\\s\"']+)[\"']?(?:\\s+#[^\\n]*)*\\n\\s+tag:\\s+[\"']?(?<currentValue>[^@\\s\"']+)(?:@(?<currentDigest>sha256:[0-9a-f]+))?[\"']?",
      ],
      registryUrlTemplate: "{{{ replace '^' 'https://' registryUrl }}}",
    },
  ]
};
